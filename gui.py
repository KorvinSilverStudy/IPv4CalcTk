#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Project: IPv4 Calculator Tk
Copyright 2017, Korvin F. Ezüst
"""

# TODO: create new combobox containing host numbers
# TODO: link comboboxes together
# ✓ make text area scrollable
# TODO: better docstrings and comments

__author__ = "Korvin F. Ezüst"
__copyright__ = "Copyright (c) 2017., Korvin F. Ezüst"
__license__ = "MIT"
__version__ = "0.2"
__email__ = "dev@korvin.eu"
__status__ = "Abandoned"

import tkinter as tk
from tkinter import ttk
from tkinter import font
import ipv4_calc_dev as ipcalc


# Building the main window
window = tk.Tk()
window.title("IPv4 Calculator")
window.geometry("{}x{}".format(700, 500))
# Making window fix sized
window.resizable(width=False, height=False)

# Adding labels
ttk.Label(window, text="IPv4 address:").place(x=10, y=10)
ttk.Label(window, text="Subnet mask:").place(x=220, y=10)

# Adding font
fn = font.Font(family="Courier", size=10)

# Adding input field
ip = tk.StringVar()
ipEntered = ttk.Entry(window, width=24, text=ip)
ipEntered.place(x=10, y=40)
ipEntered.focus()

# Adding combobox
mask = tk.StringVar()
maskEntered = ttk.Combobox(window, width=24, height=33, text=mask)
maskEntered["values"] = ("/0 - 0.0.0.0",
                         "/1 - 128.0.0.0",
                         "/2 - 192.0.0.0",
                         "/3 - 224.0.0.0",
                         "/4 - 240.0.0.0",
                         "/5 - 248.0.0.0",
                         "/6 - 252.0.0.0",
                         "/7 - 254.0.0.0",
                         "/8 - 255.0.0.0",
                         "/9 - 255.128.0.0",
                         "/10 - 255.192.0.0",
                         "/11 - 255.224.0.0",
                         "/12 - 255.240.0.0",
                         "/13 - 255.248.0.0",
                         "/14 - 255.252.0.0",
                         "/15 - 255.254.0.0",
                         "/16 - 255.255.0.0",
                         "/17 - 255.255.128.0",
                         "/18 - 255.255.192.0",
                         "/19 - 255.255.224.0",
                         "/20 - 255.255.240.0",
                         "/21 - 255.255.248.0",
                         "/22 - 255.255.252.0",
                         "/23 - 255.255.254.0",
                         "/24 - 255.255.255.0",
                         "/25 - 255.255.255.128",
                         "/26 - 255.255.255.192",
                         "/27 - 255.255.255.224",
                         "/28 - 255.255.255.240",
                         "/29 - 255.255.255.248",
                         "/30 - 255.255.255.252",
                         "/31 - 255.255.255.254",
                         "/32 - 255.255.255.255",)
maskEntered.current(24)
maskEntered.place(x=220, y=40)
maskEntered.configure(state="readonly")

# Adding apply button
def applyBtnClick():
    # Creating string variable
    out = tk.StringVar()
    out.set("")
    # Creating first text area to hold output
    outDisplay = tk.Text(window, width=83, height=18, font=fn)
    # Creating second text area to hold output
    outDisplay2 = tk.Text(window, width=83, height=6, font=fn, wrap="none")
    # Creating scrollbar for second text area
    xscroll = tk.Scrollbar(outDisplay2, orient="horizontal")
    outDisplay2.configure(xscrollcommand=xscroll.set)
    xscroll.configure(command=outDisplay2.xview)

    maskT = mask.get()
    maskP = ""
    for c in maskT:
        if c != "/":
            maskP += c
        if c == " ":
            break
    # Getting output from IP calculator script
    output = ipcalc.printout(ip.get(), maskP)
    # Setting text in first text area
    outDisplay.insert("0.0", output[0])
    # Setting text in second text area
    outDisplay2.insert("0.0", output[1])
    # Making both readonly
    outDisplay.configure(state="disabled")
    outDisplay2.configure(state="disabled")
    # Placing them on the window
    outDisplay.place(x=10, y=80)
    outDisplay2.place(x=10, y=380)
    # Placing scrollbar under second text area
    xscroll.place(x=0, y=82, width=665)

# Adding exit button
def exitBtnClick():
    window.destroy()

applyBtn = ttk.Button(window, text="Apply", command=applyBtnClick).place(x=480, y=38)
exitBtn = ttk.Button(window, text="Exit", command=exitBtnClick).place(x=580, y=38)

# Running the main window
window.mainloop()
