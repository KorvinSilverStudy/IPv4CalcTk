#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Project:    IPv4 Calculator Tk
Copyright 2017, Korvin F. Ezüst
"""

# NOTE: * Every function must return a list or a bool.
#         Exceptions are the reserved() and the printout() functions
#         which return strings.
#       * Every function must handle exceptions.
#       * Every function must have docstring and comments.
#       * The program must validate if a private IP address.
#         can be used with the given subnet mask,
#         and return information accordingly.
#       * If everything checks out,
#         the final program must return:
#           + the IP address
#           + the IP address's binary form
#           + the subnet mask's IP address
#           + the subnet mask's binary form
#           + the subnet mask's CIDR notation
#           + the subnet mask's wildcard
#           + the wildcard's binary form
#           + the network address
#           + the lowest usable IP address
#           + the highest usable IP address
#           + the broadcast address
#           + the number of usable IP addresses

# ✓ get binary IP
# ✓ get subnet mask IP from CIDR notation
# ✓ get subnet mask CIDR notation from IP
# ✓ validate the IP address and the subnet mask
# ✓ get wildcard
# ✓ get network address
# ✓ get lowest IP
# ✓ get highest IP
# ✓ get broadcast address
# ✓ get number of hosts
# TODO: check if the input private addresses matches the subnet mask
# TODO: better exception handling
# TODO: better docstrings and comments

__author__ = "Korvin F. Ezüst"
__copyright__ = "Copyright (c) 2017., Korvin F. Ezüst"
__license__ = "MIT"
__version__ = "0.9"
__email__ = "dev@korvin.eu"
__status__ = "Abandoned"


def int2list(ip):
    """
    Create a list of four integers
    from the input integer.
    This function calculates the subnet mask's
    IP address from the CIDR notation. It is assumed
    that the input is always in between 0-32. Never pass
    an int outside that range to this function!
    :param ip: int
    :return: list
    """

    def octet(i):
        """
        Returns the subnet mask octet
        corresponding to the input integer.
        The input must be between 0-8. Never pass
        an int outside that range to this function!
        :param i: int
        :return: int
        """

        if i == 0:
            return 0
        if i == 1:
            return 128
        if i == 2:
            return 192
        if i == 3:
            return 224
        if i == 4:
            return 240
        if i == 5:
            return 248
        if i == 6:
            return 252
        if i == 7:
            return 254
        if i == 8:
            return 255

    address = list()
    if ip < 8:
        address.append(octet(ip))
        address.append(0)
        address.append(0)
        address.append(0)
    if 8 <= ip < 16:
        address.append(255)
        address.append(octet(ip-8))
        address.append(0)
        address.append(0)
    if 16 <= ip < 24:
        address.append(255)
        address.append(255)
        address.append(octet(ip-16))
        address.append(0)
    if 24 <= ip:
        address.append(255)
        address.append(255)
        address.append(255)
        address.append(octet(ip-24))

    return address


def binary(ip):
    """
    Converts the given decimal IP address to binary.
    The function accepts a list of four integers
    as a valid IP address or an int or str as a valid CIDR notation
    and returns a list of four strings representing binary numbers.
    :param ip: list, int or str
    :return: list
    """

    try:
        ip = int(ip)
    except (ValueError, TypeError):
        pass

    if isinstance(ip, int):
        ip = int2list(ip)

    b = list()
    for i in ip:
        # convert each int to a string of eight
        # ones and zeroes and append it to the list
        b.append(format(i, "08b"))

    return b


def bin2list(b):
    """
    Converts a list of strings representing binary numbers
    to a list of decimal integers
    :param b: list
    :return: list
    """

    for i in range(4):
        b[i] = int(b[i], 2)

    return b


def list2int(ip):
    """
    Create an int from a list of four integers.
    This function calculates the subnet mask's
    CIDR notation from its IP address. It is assumed
    that the input is always a list and a valid subnet mask.
    :param ip: list
    :return: int
    """

    # Use the binary() function to get the binary
    # form of the address and count the ones in it.
    ip = binary(ip)
    count = 0
    for i in ip:
        for j in i:
            if j == "1":
                count += 1

    return count


def str2ip(ip):
    """
    Generates a list of four integers
    from an input string.
    If the input is not a valid IPv4 address,
    the return list will be [-1, -1, -1, -1]
    :param ip: str
    :return: list of four integers
    """

    invalid = [-1, -1, -1, -1]
    try:
        # If the input is a string that can be
        # converted to an integer, then it's a subnet mask.
        # Calculate its IP address using the number.
        ip = int(ip)
        # Check if it's a valid CIDR notation.
        if 0 <= ip <= 32:
            # If it is, get the IP address as a list of four integers
            ip = int2list(ip)
            # and check if it's a valid IP address
            if valid(ip):
                return ip
            else:
                return invalid
        else:
            return invalid
    except (ValueError, TypeError):
        try:
            # If the input is a string that can be
            # converted to a list of four integers
            # it's an IP address.

            address = list()
            tmp = ""

            # Convert the string to list
            for c in ip:
                if c != ".":
                    tmp += c
                else:
                    address.append(int(tmp))
                    tmp = ""

            # The code above misses the fourth octet, adding it here
            address.append(int(tmp))

            # Return it if it's a valid IP address
            if valid(address):
                return address
            else:
                return invalid
        except:
            return invalid


def inverse(ip):
    """
    Inverses the IP address, e.g. gets the wildcard
    of a subnet mask.
    The function accepts a list of four integers
    or a single integer and returns a list of four
    integers. Do not pass other input to this function!
    :param ip: list or int
    :return: list
    """

    # Using exception handling so it won't matter
    # if the input is actually of the type int or str.
    # Tries to convert the input to an int.
    try:
        ip = int(ip)
        # converts the int to a list using the int2list() function
        ip = int2list(ip)

        inv = list()
        for i in ip:
            inv.append(255-i)

        return inv
    # Catches TypeError and continues
    # treating the input as a list
    except (ValueError, TypeError):
        inv = list()
        for i in ip:
            inv.append(255-i)

        return inv


def valid(ip):
    """
    Check if the provided IP address is valid.
    The input must be a list of four integers
    and the output will be either True or False
    :param ip: list, list of four integers
    :return: bool
    """

    if len(ip) != 4:
        return False

    for i in ip:
        if i > 255 or i < 0:
            return False

    return True


def validMask(mask):
    """
    Check if the provided IP address is a valid subnet mask.
    The input must be a list of four integers
    and the output will be either True or False
    :param mask: list
    :return: bool
    """

    # Make a variable that contains all the valid octets
    # in a subnet mask as integers
    octets = (0, 128, 192, 224, 240, 248, 252, 254, 255)

    # Return False if an octet is bigger
    # than the octet before it
    for i in range(len(mask)-1):
        if mask[i] < mask[i+1] or ((mask[i] != 255 and mask[i] != 0) and mask[i] == mask[i+1]):
            return False

    # Return false if any of the octets is invalid
    for i in mask:
        if i not in octets:
            return False

    return True


def network(ip, mask):
    """
    Calculates the IP address of a network
    based on the given IP address and subnet mask.
    The function accepts the IP as a list of four integers
    and the mask as an int or a list of four integers.
    Do not pass any other values to it!
    The network address will be returned as a list of
    four integers.
    :param ip: list
    :param mask: list or int
    :return: tuple(list, int)
    """

    # Converts ip and mask to binary
    ip = binary(ip)
    mask = binary(mask)

    net = list()
    octet = ""
    for i in range(4):
        for j in range(8):
            if mask[i][j] == "1":
                octet += ip[i][j]
            else:
                octet += "0"
        net.append(octet)
        octet = ""

    # Counts the zeroes of the subnet mask
    # to get the CIDR notation
    sn = 0
    for i in mask:
        for j in i:
            if j == "1":
                sn += 1

    return bin2list(net), sn


def lowest(ip, mask):
    """
    Calculates the first usable IP address of the network
    from a given IP address and subnet mask.
    Assumes that the IP and mask are valid.
    :param ip: list
    :param mask: list, int or str
    :return: list
    """

    # The lowest IP is always the next IP after the network address
    i = network(ip, mask)[0]
    i[3] += 1
    return i


def hosts(ip, mask):
    """
    Calculates how many usable IP addresses
    are there in a given network.
    The function accepts a list of four integers
    as the IP address and a list, an int or a str
    as the subnet mask. Always assumes they're valid.
    :param ip: list
    :param mask: list, int, str
    :return: int
    """

    try:
        mask = int(mask)
        return 2 ** (32 - mask) - 2
    except:
        mask = list2int(mask)
        return 2 ** (32 - mask) - 2


def broadcast(ip, mask):
    """
    Calculates the broadcast address of the network
    from a given IP address and subnet mask.
    Assumes that the IP and masks are valid.
    :param ip: list
    :param mask: list, int or str
    :return: list
    """

    # Using the binary() and network() functions
    # to get the binary form of the network address and subnet mask
    ip = binary(network(ip, mask)[0])
    mask = binary(mask)

    # Create a new list of four strings representing binary numbers
    # by adding together the two lists.
    br = list()
    octet = ""
    for i in range(4):
        for j in range(8):
            # Where the digits match, the broadcast address's digit will be one
            if ip[i][j] == mask[i][j]:
                octet += "1"
            # where they don't match, it will be zero
            else:
                octet += "0"
        br.append(octet)
        octet = ""

    # Return a list got with the bin2list() function
    return bin2list(br)


def highest(ip, mask):
    """
    Calculates the last usable IP address of a network
    from a given IP address and subnet mask.
    Assumes that the IP and mask are valid.
    :param ip: list
    :param mask: list, int or str
    :return: list
    """

    # The highest IP is always the IP before the broadcast address
    i = broadcast(ip, mask)
    i[3] -= 1

    return i


def reserved(ip, mask=0):
    """
    Check if the provided IP address is a reserved address.
    The input is a list of four integers and a valid IP address.
    Source: * https://en.wikipedia.org/wiki/Reserved_IP_addresses
            * https://www.iana.org/assignments/iana-ipv4-special-registry/iana-ipv4-special-registry.xhtml
    :param ip: list
    :return: str
    """

    more = "\n\n\t\tFor more information visit:\n" \
           "\t\t* https://en.wikipedia.org/wiki/Reserved_IP_addresses\n " \
           "\t\t* https://www.iana.org/assignments/iana-ipv4-special-registry/iana-ipv4-special-registry.xhtml"

    mres = "This address block might overlap with a reserved address block."

    # Used for local communications within a private network
    if ip[0] == 10:
        if mask >= 8:
            return "Address block used for local communications within a private network." + more
        else:
            return mres + more
    if ip[0] == 172 and 16 <= ip[1] <= 31:
        if mask >= 16:
            return "Address block used for local communications within a private network." + more
        else:
            return mres + more
    if ip[0] == 192 and ip[1] == 168:
        if mask >= 16:
            return "Address block used for local communications within a private network." + more
        else:
            return mres + more

    # Used for communications between a service provider
    # and its subscribers when using a carrier-grade NAT
    if ip[0] == 100 and 64 <= ip[1] <= 127:
        if mask >= 10:
            return "Address block used for communications between a service provider and its subscribers when using" \
                "a carrier-grade NAT." + more
        else:
            return mres + more

    # IPv4 Service Continuity Prefix
    if ip[0] == 192 and ip[1] == 0 and ip[2] == 0:
        if mask >= 29:
            return "IPv4 Service Continuity Prefix" + more
        else:
            return mres + more

    # Used for the IANA IPv4 Special Purpose Address Registry
    if ip[0] == 192 and ip[1] == 0 and ip[2] == 0 and 0 <= ip[3] <= 255:
        if mask >= 24:
            return "Address block used for the IANA IPv4 Special Purpose Address Registry." + more
        else:
            return mres + more

    # Used for testing of inter-network communications between two separate subnets
    if ip[0] == 198 and 18 <= ip[1] <= 19:
        if mask >= 15:
            return "Address block used for testing of inter-network communications between two separate subnets." + more
        else:
            return mres + more

    # Used for broadcast messages to the current ("this")
    if ip[0] == 0 and ip[1] == 0 and \
       ip[2] == 0 and ip[3] == 0:
        if mask >= 8:
            return "Used for broadcast messages to the current (\"this\")." + more
        else:
            return mres + more

    # Used for loopback addresses to the local host
    if ip[0] == 127:
        if mask >= 8:
            return "Used for loopback addresses to the local host." + more
        else:
            return mres + more

    # Used for link-local addresses between two hosts
    # on a single link when no IP address is otherwise
    # specified, such as would have normally been retrieved
    # from a DHCP server
    if ip[0] == 169 and ip[1] == 254:
        if mask >= 16:
            return "Used for link-local addresses between two hosts on a single link when no IP address is otherwise" \
                "specified, such as would have normally been retrieved from a DHCP server." + more
        else:
            return mres + more

    # IPv4 dummy address
    if ip[0] == 192 and ip[1] == 0 and \
        ip[2] == 0 and ip[3] == 8 and mask == 32:
        return "IPv4 dummy address"

    # Port Control Protocol Anycast
    if ip[0] == 192 and ip[1] == 0 and \
        ip[2] == 0 and ip[3] == 9 and mask == 32:
        return "Port Control Protocol Anycast"

    # Traversal Using Relays around NAT Anycast
    if ip[0] == 192 and ip[1] == 0 and \
        ip[2] == 0 and ip[3] == 10 and mask == 32:
        return "Traversal Using Relays around NAT Anycast"

    # NAT64/DNS64 Discovery
    if ip[0] == 192 and ip[1] == 0 and \
        ip[2] == 0 and 170 <= ip[3] <= 171 and mask == 32:
        return "NAT64/DNS64 Discovery"

    # Assigned as "TEST-NET-1" for use in documentation
    # and examples. It should not be used publicly
    if ip[0] == 192 and ip[1] == 0 and ip[2] == 2 and 0<= ip[3] <= 255:
        if mask >= 24:
            return "Assigned as \"TEST-NET-1\" for use in documentation and examples. It should not be used publicly." \
                + more
        else:
            return mres + more

    # AS112-v4
    if ip[0] == 192 and ip[1] == 31 and ip[2] == 196:
        if mask >= 24:
            return "AS112-v4" + more
        else:
            return mres + more

    # AMT
    if ip[0] == 192 and ip[1] == 52 and ip[2] == 193:
        if mask >= 24:
            return "AMT" + more
        else:
            return mres + more

    # Used by 6to4 anycast relays (deprecated)
    # if ip[0] == 192 and ip[1] == 88 and ip[2] == 99:
    #     if mask >= 24:
    #         return "Used by 6to4 anycast relays (deprecated)"
    #     else:
    #         return mres + more

    # Assigned as "TEST-NET-2" for use in documentation and examples.
    # It should not be used publicly.
    if ip[0] == 198 and ip[1] == 51 and ip[2] == 100:
        if mask >= 24:
            return "Assigned as \"TEST-NET-2\" for use in documentation and examples.It should not be used publicly." \
                + more
        else:
            return mres + more

    # Assigned as "TEST-NET-3" for use in documentation and examples.
    # It should not be used publicly.
    if ip[0] == 203 and ip[1] == 0 and ip[2] == 113:
        if mask >= 24:
            return "Assigned as \"TEST-NET-3\" for use in documentation and examples. It should not be used publicly." \
                + more
        else:
            return mres + more

    # Multicast addresses
    if 224 <= ip[0] <= 239:
        if mask >= 4:
            return "Multicast address." + more
        else:
            return mres + more

    # Reserved for the "limited broadcast" destination address
    if ip[0] == 255 and ip[1] == 255 and \
       ip[2] == 255 and ip[3] == 255 and mask == 32:
        return "Reserved for the \"limited broadcast\" destination address." + more

    # Reserved for future use
    if 240 <= ip[0] <= 255:
        if mask >= 4:
            return "Reserved for future use." + more
        else:
            return mres + more

    return "--"


def list2str(ip):
    """
    Makes a string from a list.
    Assumes that the input list is an IP address.
    :param ip: list
    :return: str
    """

    s = ""
    for i in range(4):
        s += str(ip[i])
        if i != 3:
            s += "."

    return s


def printout(ip, mask="24"):
    ip_list = str2ip(ip)
    mask_list = str2ip(mask)
    mask_cidr = list2int(mask_list)
    ip_bin = binary(ip_list)
    mask_bin = binary(mask_list)
    wild = inverse(mask_list)
    wild_bin = binary(wild)
    net = network(ip_list, mask_list)[0]
    first = lowest(ip_list, mask_list)
    last = highest(ip_list, mask_list)
    br = broadcast(ip_list, mask_list)
    av = hosts(ip_list, mask_list)

    out = "\n"
    note = ""
    nl = "\n"
    if valid(ip_list) and validMask(mask_list):
        out += "IP address:".ljust(20) + ip.ljust(20) + list2str(ip_bin) + nl
        out += "Subnet mask:".ljust(20) + list2str(mask_list).ljust(20) + list2str(mask_bin) + "    /" + str(mask_cidr) + nl
        out += "Wildcard:".ljust(20) + list2str(wild).ljust(20) + list2str(wild_bin) + nl + nl

        if mask_cidr > 30:
            out += "Maximum number of IP addresses:".ljust(32) + "0" + nl
            out += "Maximum number of subnets:".ljust(32) + "0" + nl + nl

            note += "Note:\t" + reserved(ip_list, mask_cidr) + nl
        else:
            out += "Network address:".ljust(20) + (list2str(net) + "/" + str(mask_cidr)).ljust(20) + list2str(binary(net)) + nl
            out += "First usable IP:".ljust(20) + list2str(first).ljust(20) + list2str(binary(first)) + nl
            out += "Last usable IP:".ljust(20) + list2str(last).ljust(20) + list2str(binary(last)) + nl
            out += "Broadcast address:".ljust(20) + list2str(br).ljust(20) + list2str(binary(br)) + nl + nl

            out += "Maximum number of IP addresses:".ljust(32) + str(av) + nl
            out += "Maximum number of subnets:".ljust(32) + str(av+2) + nl + nl

            note += "Note:\t" + reserved(ip_list, mask_cidr) + nl
    elif valid(ip_list) and not validMask(mask_list):
        out += "IP address:".ljust(20) + ip.ljust(20) + list2str(ip_bin) + nl
        out += "Invalid subnet mask" + nl
    elif not valid(ip_list) and validMask(mask_list):
        out += "Invalid IP address" + nl
        out += "Subnet mask:".ljust(20) + list2str(mask_list).ljust(20) + list2str(mask_bin) + "    /" + str(mask_cidr) + nl
        out += "Wildcard:".ljust(20) + list2str(wild).ljust(20) + list2str(wild_bin) + nl
    else:
        out += "Invalid IP address and subnet mask" + nl

    out += "\n\n** Copyright (c) 2017., Korvin F. Ezüst **\n**    Released under the MIT license    **"

    # print(out)

    return out, note


if __name__ == '__main__':
    ip = input("IPv4 address: ")
    mask = input("Subnet mask: ")

    ip_list = str2ip(ip)
    mask_list = str2ip(mask)
    mask_cidr = list2int(mask_list)
    ip_bin = binary(ip_list)
    mask_bin = binary(mask_list)
    wild = inverse(mask_list)
    wild_bin = binary(wild)
    net = network(ip_list, mask_list)[0]
    first = lowest(ip_list, mask_list)
    last = highest(ip_list, mask_list)
    br = broadcast(ip_list, mask_list)
    av = hosts(ip_list, mask_list)

    if valid(ip_list) and validMask(mask_list):
        print("IP address:".ljust(20), ip.ljust(20), list2str(ip_bin))
        print("Subnet mask:".ljust(20), list2str(mask_list).ljust(20), list2str(mask_bin), "   /" + str(mask_cidr))
        print("Wildcard:".ljust(20), list2str(wild).ljust(20), list2str(wild_bin))
        print()
        if mask_cidr > 30:
            print("Maximum number of IP addresses:".ljust(32), 0)
            print("Maximum number of subnets:".ljust(32), 0)
            print()
            print("Note:\t", reserved(ip_list, mask_cidr))
        else:
            print("Network address:".ljust(20), (list2str(net) + "/" + str(mask_cidr)).ljust(20), list2str(binary(net)))
            print("First usable IP:".ljust(20), list2str(first).ljust(20), list2str(binary(first)))
            print("Last usable IP:".ljust(20), list2str(last).ljust(20), list2str(binary(last)))
            print("Broadcast address:".ljust(20), list2str(br).ljust(20), list2str(binary(br)))
            print()
            print("Maximum number of IP addresses:".ljust(32), av)
            print("Maximum number of subnets:".ljust(32), av+2)
            print()
            print("Note:\t" + reserved(ip_list, mask_cidr))
    elif valid(ip_list) and not validMask(mask_list):
        print("IP address:".ljust(20), ip.ljust(20), list2str(ip_bin))
        print("Invalid subnet mask")
    elif not valid(ip_list) and validMask(mask_list):
        print("Invalid IP address")
        print("Subnet mask:".ljust(20), mask.ljust(20), list2str(mask_bin), "   /" + str(mask_cidr))
        print("Wildcard:".ljust(20), list2str(wild).ljust(20), list2str(wild_bin))
    else:
        print("Invalid IP address and subnet mask")

    print("\n\n** Copyright (c) 2017., Korvin F. Ezüst **\n**    Released under the MIT license    **")
